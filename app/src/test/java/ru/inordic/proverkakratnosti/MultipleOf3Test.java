package ru.inordic.proverkakratnosti;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MultipleOf3Test {

    @Test
    void checkMultipleOf3() {
        MultipleOf3 multipleOf3 = new MultipleOf3();
        Assertions.assertTrue(multipleOf3.check(new char[]{ '9', '3'}));
        Assertions.assertFalse(multipleOf3.check(new char[]{ '8', '3'}));

    }
}