package ru.inordic.proverkakratnosti;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EvenNumberTest {

    @Test
    void check() {


        EvenNumber evenNumber = new EvenNumber();
        Assertions.assertTrue(evenNumber.check(new char[]{ '9', '8'}));
        Assertions.assertFalse(evenNumber.check(new char[]{ '8', '9'}));
    }
}