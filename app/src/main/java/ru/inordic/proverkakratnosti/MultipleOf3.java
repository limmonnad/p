package ru.inordic.proverkakratnosti;

import java.util.ArrayList;

/**
 * кратность 3
 */

public class MultipleOf3 implements Del {
    @Override
    public String printMultiple() {
        return "кратно 3";
    }

    @Override
    public boolean check(char[] x) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        while (x.length != 1) {                               // МАССИВ С ОДНИМ ЗНАЧЕНИЕМ, ПРОВЕРЯЕМ НЕСКОЛЬКО РАЗ
            arrayList.clear();
            arrayList = fromCharArrayToListInteger(x);      // ЧТОБЫ СЛОЖИТЬ ЗНАЧЕНИЯ
            x = fromListIntegerToCharArray(arrayList);     // СКЛАДЫВАЕМ ЗНАЧЕНИЯ И В МАССИВ CHAR
        }

        if (x[0] == '3' || x[0] == '6' || x[0] == '9') {
            return true;
        } else {
            return false;
        }
    }

    // СКЛАДЫВАЕМ ЗНАЧЕНИЯ И В МАССИВ CHAR
    private char[] fromListIntegerToCharArray(ArrayList<Integer> arrayList) {
        int sum = 0;
        for (int i = 0; i < arrayList.size(); i++) {
            sum += arrayList.get(i);
        }
        char[] c = Integer.toString(sum).toCharArray();
        return c;
    }

    // ЧТОБЫ СЛОЖИТЬ ЗНАЧЕНИЯ
    private ArrayList<Integer> fromCharArrayToListInteger(char[] x) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        for (int i = 0; i < x.length; i++) {
            arrayList.add(Character.getNumericValue(x[i]));
        }
        return arrayList;
    }


}







//
//public class Del3 {
//
//    public void kratno3(char[] x) {
//
//        ArrayList<Integer> arrayList = getIntegersArrayList(x);
//        char[] c = fromListToCharArray(arrayList);
//
//        while (c.length != 1) {
//
//            arrayList.clear();
//            arrayList = getIntegersArrayList(c);
//            c = fromListToCharArray(arrayList);
//        }
//
//        if (c[0] == '3' || c[0] == '6' || c[0] == '9') {
//            System.out.println("кратно 3");
//        } else
//            System.out.println("не кратно 3");
//    }
//
//
//    CharArray charArray = new CharArray(); // объект класса массив
//
//    private char[] fromListToCharArray(ArrayList<Integer> arrayList) {
//        int sum = 0;
//        for (int i = 0; i < arrayList.size(); i++) {
//            sum += arrayList.get(i);
//        }
//        char[] c = charArray.toChar(sum);
//        return c;
//    }
//
//    private ArrayList<Integer> getIntegersArrayList(char[] x) {
//        ArrayList<Integer> arrayList = new ArrayList<>();
//        for (int i = 0; i < x.length; i++) {
//            arrayList.add(Character.getNumericValue(x[i]));
//        }
//        return arrayList;
//    }
//
//}
