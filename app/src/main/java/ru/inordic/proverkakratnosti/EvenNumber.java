package ru.inordic.proverkakratnosti;

public class EvenNumber implements Del {


    @Override
    public String printMultiple() {
        return "четное";
    }

    @Override
    public boolean check(char[] x) {
        char c = x[x.length - 1];
        if (c == '0' || c == '2' || c == '4' || c == '6' || c == '8') {
            return true;
        } else {
            return false;
        }
    }



}
