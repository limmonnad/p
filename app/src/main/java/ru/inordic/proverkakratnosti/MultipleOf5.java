package ru.inordic.proverkakratnosti;

public class MultipleOf5 implements Del {

    @Override
    public String printMultiple() {
        return "кратно 5";
    }
    @Override
    public boolean check(char[] x) {
        char c = x[x.length - 1];
        if (c == '0' || c == '5') {
            return true;
        } else {
            return false;
        }

    }
}

