package ru.inordic.proverkakratnosti;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MultipleOf5Test {

    @Test
    void check() {


        MultipleOf5 multipleOf5 = new MultipleOf5();
        Assertions.assertTrue(multipleOf5.check(new char[]{ '1', '0'}));
        Assertions.assertFalse(multipleOf5.check(new char[]{ '1', '9'}));


    }
}