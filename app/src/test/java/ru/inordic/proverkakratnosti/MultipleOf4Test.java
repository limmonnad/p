package ru.inordic.proverkakratnosti;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MultipleOf4Test {

    @Test
    void checkMultipleOf4() {

            MultipleOf4 multipleOf4 = new MultipleOf4();
            Assertions.assertTrue(multipleOf4.check(new char[]{ '6', '8'}));
            Assertions.assertFalse(multipleOf4.check(new char[]{ '6', '9'}));






    }
}