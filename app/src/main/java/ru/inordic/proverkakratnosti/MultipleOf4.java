package ru.inordic.proverkakratnosti;

public class MultipleOf4 implements Del {


    @Override
    public String printMultiple() {
        return "кратно 4";
    }

    @Override
    public boolean check(char[] x) {

        StringBuilder sb = new StringBuilder();

        if (x.length < 2) {
            if (x[0] == '4' || x[0] == '8') {
                return true;
            } else
                return false;
        } else {
            sb.append((x[x.length - 2]));
            sb.append((x[x.length - 1]));
            int tmp = Integer.parseInt(sb.toString());

            int zero;
            do {
                tmp -= 4;
                zero = tmp;
                if (zero == 0) {
                    return true;
                }
            } while (zero > 0);

            if (zero < 0) {
                return false;
            }
        }

        return false;
    }
}
